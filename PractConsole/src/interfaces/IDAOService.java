package interfaces;

import console.models.Genre;
import console.models.Movie;
import console.models.Producer;

import java.util.ArrayList;

public interface IDAOService
{
    //region AddMethods

    public boolean addMovie(Movie movie);

    public boolean addGenre(String genreName);

    public boolean addProducer(String producerName);

    //endregion

    //region SelectMethods

    public Genre getGenreById(int genreId);

    public Producer getProducerById(int producerId);

    public int getMovieIdByName(String movieName);

    public int getGenreIdByName(String genreName);

    public int getProducerIdByName(String producerName);

    public Movie getMovieById(int movieId);

    public Movie getMovieByName(String movieName);

    public ArrayList<Genre> getMovieGenres(int movieId);

    public ArrayList<Producer> getMovieProducers(int movieId);

    public ArrayList<Movie> getAllMovies();

    public ArrayList<Genre> getAllGenres();

    public ArrayList<Producer> getAllProducers();

    //endregion

    //region DeleteMethods

    public boolean deleteMovieByName(String movieName);

    public boolean deleteGenreByName(String genreName);

    public boolean deleteProducerByName(String producerName);

    //endregion
}
