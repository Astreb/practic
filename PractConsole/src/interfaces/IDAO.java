package interfaces;

import console.models.Genre;
import console.models.Movie;
import console.models.Producer;
import main.entity.GenreEntity;
import main.entity.MovieEntity;
import main.entity.ProducerEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public interface IDAO
{
    //region AddMethods

    public boolean addMovie(MovieEntity movie);

    public boolean addGenre(GenreEntity genre);

    public boolean addProducer(ProducerEntity name);

    //endregion

    //region SelectMethods

    public GenreEntity getGenreById(int genreId);

    public ProducerEntity getProducerById(int producerId);

    public int getMovieIdByName(String movieName);

    public int getGenreIdByName(String genreName);

    public int getProducerIdByName(String producerName);

    public MovieEntity getMovieById(int movieId);

    public MovieEntity getMovieByName(String movieName);

    public ArrayList<GenreEntity> getMovieGenres(int movieId);

    public ArrayList<ProducerEntity> getMovieProducers(int movieId);

    public ArrayList<MovieEntity> getAllMovies();

    public ArrayList<GenreEntity> getAllGenres();

    public ArrayList<ProducerEntity> getAllProducers();

    //endregion

    //region DeleteMethods

    public boolean deleteMovieByName(String movieName);

    public boolean deleteGenreByName(String genreName);

    public boolean deleteProducerByName(String producerName);

    //endregion
}
