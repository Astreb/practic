package main;

import main.entity.*;
import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.Map;

public class HibernateSessionFactoryClass {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryClass() {}

    public static SessionFactory getSessionFactory()
    {
        if (sessionFactory == null)
        {
            try
            {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(GenreEntity.class);
                configuration.addAnnotatedClass(MovieEntity.class);
                configuration.addAnnotatedClass(ProducerEntity.class);

                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            }

            catch (Exception ex)
            {
                System.out.println(ex);
            }
        }

        return sessionFactory;
    }
    /* static
    {
        try
        {
            Configuration configuration = new Configuration().configure();
            configuration.addAnnotatedClass(GenreEntity.class);
            configuration.addAnnotatedClass(MovieEntity.class);
            configuration.addAnnotatedClass(ProducerEntity.class);
            configuration.addAnnotatedClass(GenreMovieEntity.class);
            configuration.addAnnotatedClass(ProducerMovieEntity.class);


            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory(builder.build());
        }

        catch (Throwable ex)
        {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException
    {
        return sessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception
    {
        final Session session = getSession();

        try
        {
            System.out.println("querying all the managed entities...");
            final Metamodel metamodel = session.getSessionFactory().getMetamodel();
            for (EntityType<?> entityType : metamodel.getEntities())
            {
                final String entityName = entityType.getName();
                final Query query = session.createQuery("from " + entityName);
                System.out.println("executing: " + query.getQueryString());
                for (Object o : query.list())
                {
                    System.out.println("  " + o);
                }
            }
        }

        finally
        {
            session.close();
        }
    }*/
}