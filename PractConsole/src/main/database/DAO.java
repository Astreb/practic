package main.database;

import console.models.Genre;
import console.models.Movie;
import console.models.Producer;
import interfaces.IDAO;
import main.HibernateSessionFactoryClass;
import main.entity.*;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;

public class DAO implements IDAO
{
    //region AddMethods
    @Override
    public boolean addMovie(MovieEntity movie)
    {
        try
        {
            Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
            Transaction tx1 = session.beginTransaction();
            session.save(movie);
            tx1.commit();
            session.close();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при добавлении фильма в базу данных");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean addGenre(GenreEntity genre)
    {
        try
        {
            Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
            Transaction tx1 = session.beginTransaction();
            session.save(genre);
            tx1.commit();
            session.close();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при добавлении жанра в базу данных");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean addProducer(ProducerEntity producer)
    {
        try
        {
            Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
            Transaction tx1 = session.beginTransaction();
            session.save(producer);
            tx1.commit();
            session.close();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при добавлении режиссёра в базу данных");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    //endregion

    //region SelectMethods
    @Override
    public GenreEntity getGenreById(int genreId)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        return session.get(GenreEntity.class, genreId);
    }

    @Override
    public ProducerEntity getProducerById(int producerId)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        return session.get(ProducerEntity.class, producerId);
    }

    @Override
    public int getMovieIdByName(String movieName)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(MovieEntity.class);
        criteria.add(Restrictions.eq("name", movieName));
        MovieEntity movie = (MovieEntity) criteria.uniqueResult();
        return movie.getId();
    }

    @Override
    public int getGenreIdByName(String genreName)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(GenreEntity.class);
        criteria.add(Restrictions.eq("name", genreName));
        GenreEntity genre = (GenreEntity) criteria.uniqueResult();
        return genre.getId();
    }

    @Override
    public int getProducerIdByName(String producerName)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(ProducerEntity.class);
        criteria.add(Restrictions.eq("name", producerName));
        ProducerEntity producer = (ProducerEntity) criteria.uniqueResult();
        return producer.getId();
    }

    @Override
    public MovieEntity getMovieById(int movieId)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        return session.get(MovieEntity.class, movieId);
    }

    @Override
    public MovieEntity getMovieByName(String movieName)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(MovieEntity.class);
        criteria.add(Restrictions.eq("name", movieName));
        MovieEntity movie = (MovieEntity) criteria.uniqueResult();
        return movie;
    }

    @Override
    public ArrayList<GenreEntity> getMovieGenres(int movieId)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        MovieEntity movie = session.get(MovieEntity.class, movieId);

        return new ArrayList<GenreEntity>(movie.getMovieGenres());
    }

    @Override
    public ArrayList<ProducerEntity> getMovieProducers(int movieId)
    {
        Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
        MovieEntity movie = session.get(MovieEntity.class, movieId);

        return new ArrayList<ProducerEntity>(movie.getMovieProducers());
    }

    @Override
    public ArrayList<MovieEntity> getAllMovies()
    {
        ArrayList<MovieEntity> movies = (ArrayList<MovieEntity>)  HibernateSessionFactoryClass.getSessionFactory().openSession().createQuery("FROM MovieEntity ").list();
        return movies;
    }

    @Override
    public ArrayList<GenreEntity> getAllGenres()
    {
        ArrayList<GenreEntity> genres = (ArrayList<GenreEntity>)  HibernateSessionFactoryClass.getSessionFactory().openSession().createQuery("FROM GenreEntity ").list();
        return genres;
    }

    @Override
    public ArrayList<ProducerEntity> getAllProducers()
    {
        ArrayList<ProducerEntity> producers = (ArrayList<ProducerEntity>)  HibernateSessionFactoryClass.getSessionFactory().openSession().createQuery("FROM ProducerEntity ").list();
        return producers;
    }
    //endregion

    //region DeleteMethods
    @Override
    public boolean deleteMovieByName(String movieName)
    {
        try
        {
            Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
            MovieEntity movie = session.get(MovieEntity.class, getMovieIdByName(movieName));

            Transaction tx1 = session.beginTransaction();
            session.delete(movie);
            tx1.commit();
            session.close();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при удалении фильма из базы данных");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean deleteGenreByName(String genreName)
    {
        try
        {
            Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
            GenreEntity genre = session.get(GenreEntity.class, getGenreIdByName(genreName));

            Transaction tx1 = session.beginTransaction();
            session.save(genre);
            tx1.commit();
            session.close();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при удалении жанра из базы данных");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean deleteProducerByName(String producerName)
    {
        try
        {
            Session session = HibernateSessionFactoryClass.getSessionFactory().openSession();
            ProducerEntity producer = session.get(ProducerEntity.class, getProducerIdByName(producerName));

            Transaction tx1 = session.beginTransaction();
            session.save(producer);
            tx1.commit();
            session.close();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при удалении режиссёра из базы данных");
            System.out.println(ex);
            return false;
        }

        return true;
    }
    //endregion
}
