package main.database;

import console.models.Genre;
import console.models.Movie;
import console.models.Producer;
import interfaces.IDAOService;
import main.entity.*;

import java.util.ArrayList;
import java.util.Set;

public class DAOService implements IDAOService
{
    private DAO dao = new DAO();

    //region AddMethods
    @Override
    public boolean addMovie(Movie movie)
    {
        MovieEntity movieEntity = new MovieEntity(movie);

        for (Genre genre: movie.genres)
        {
            GenreEntity genreEntity = new GenreEntity(genre);
            movieEntity.addGenre(genreEntity);
        }

        for (Producer producer:movie.producers)
        {
            ProducerEntity producerEntity = new ProducerEntity(producer);
            movieEntity.addProducer(producerEntity);
        }

        dao.addMovie(movieEntity);
        return true;
    }

    @Override
    public boolean addGenre(String genreName)
    {
        return dao.addGenre(new GenreEntity(new Genre(genreName)));
    }

    @Override
    public boolean addProducer(String producerName)
    {
        return dao.addProducer(new ProducerEntity(new Producer(producerName)));
    }
    //endregion

    //region SelectMethods
    @Override
    public Genre getGenreById(int genreId)
    {
        return new Genre(dao.getGenreById(genreId));
    }

    @Override
    public Producer getProducerById(int producerId)
    {
        return new Producer(dao.getProducerById(producerId));
    }

    @Override
    public int getMovieIdByName(String movieName)
    {
        return dao.getMovieIdByName(movieName);
    }

    @Override
    public int getGenreIdByName(String genreName)
    {
        return dao.getGenreIdByName(genreName);
    }

    @Override
    public int getProducerIdByName(String producerName)
    {
        return dao.getProducerIdByName(producerName);
    }

    @Override
    public Movie getMovieById(int movieId)
    {
        return new Movie(dao.getMovieById(movieId));
    }

    @Override
    public Movie getMovieByName(String movieName)
    {
        return new Movie(dao.getMovieByName(movieName));
    }

    @Override
    public ArrayList<Genre> getMovieGenres(int movieId)
    {
        ArrayList<GenreEntity> genresEntity = dao.getMovieGenres(movieId);
        ArrayList<Genre> genres = new ArrayList<Genre>();

        for (GenreEntity genreEntity: genresEntity)
            genres.add(new Genre(genreEntity));

        return genres;
    }

    @Override
    public ArrayList<Producer> getMovieProducers(int movieId)
    {
        ArrayList<ProducerEntity> producersEntitity = dao.getMovieProducers(movieId);
        ArrayList<Producer> producers = new ArrayList<Producer>();

        for (ProducerEntity producerEntity: producersEntitity)
            producers.add(new Producer(producerEntity));

        return producers;

    }

    @Override
    public ArrayList<Movie> getAllMovies()
    {
        ArrayList<MovieEntity> moviesEntitity = dao.getAllMovies();
        ArrayList<Movie> movies = new ArrayList<Movie>();

        for (MovieEntity movieEntity: moviesEntitity)
            movies.add(new Movie(movieEntity));

        return movies;
    }

    @Override
    public ArrayList<Genre> getAllGenres()
    {
        ArrayList<GenreEntity> genresEntity = dao.getAllGenres();
        ArrayList<Genre> genres = new ArrayList<Genre>();

        for (GenreEntity genreEntity: genresEntity)
            genres.add(new Genre(genreEntity));

        return genres;
    }

    @Override
    public ArrayList<Producer> getAllProducers()
    {
        ArrayList<ProducerEntity> producersEntitity = dao.getAllProducers();
        ArrayList<Producer> producers = new ArrayList<Producer>();

        for (ProducerEntity producerEntity: producersEntitity)
            producers.add(new Producer(producerEntity));

        return producers;
    }
    //endregion

    //region DeleteMethods
    @Override
    public boolean deleteMovieByName(String movieName)
    {
        return dao.deleteMovieByName(movieName);
    }

    @Override
    public boolean deleteGenreByName(String genreName)
    {
        return dao.deleteGenreByName(genreName);
    }

    @Override
    public boolean deleteProducerByName(String producerName)
    {
        return dao.deleteProducerByName(producerName);
    }
    //endregion
}
