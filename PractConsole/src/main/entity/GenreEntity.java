package main.entity;

import console.models.Genre;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "genre", schema = "pract")
public class GenreEntity
{
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "NAME", nullable = false, length = 20)
    private String name;

    @ManyToMany(mappedBy = "movieGenres")
    private Set<MovieEntity> genreMovies;

    //region Getters

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Set<MovieEntity> getGenreMovies()
    {
        return genreMovies;
    }

    //endregion

    //region Setters

    public void setId(int id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setGenreMovies(Set<MovieEntity> genreMovies)
    {
        this.genreMovies = genreMovies;
    }

    //endregion

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenreEntity that = (GenreEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name);
    }

    //region Constructors
    public GenreEntity()
    {
        this.genreMovies = new HashSet<MovieEntity>();
    }

    public GenreEntity(Genre genre)
    {
        this.id = genre.id;
        this.name = genre.name;
        this.genreMovies = new HashSet<MovieEntity>();
    }
    //endregion
}
