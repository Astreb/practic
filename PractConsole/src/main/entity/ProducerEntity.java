package main.entity;

import console.models.Producer;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "producer", schema = "pract")
public class ProducerEntity
{
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "NAME", nullable = false, length = 20)
    private String name;

    @ManyToMany(mappedBy = "movieProducers")
    private Set<MovieEntity> producerMovies;

    //region Getters
    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Set<MovieEntity> getProducerMovies()
    {
        return producerMovies;
    }

    //endregion

    //region Setters

    public void setId(int id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setProducerMovies(Set<MovieEntity> producerMovies)
    {
        this.producerMovies = producerMovies;
    }

    //endregion

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProducerEntity that = (ProducerEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name);
    }

    //region Constructors
    public ProducerEntity()
    {
        this.producerMovies = new HashSet<MovieEntity>();
    }

    public ProducerEntity(Producer producer)
    {
        this.id = producer.id;
        this.name = producer.name;

        this.producerMovies = new HashSet<MovieEntity>();
    }
    //endregion
}
