package main.entity;

import console.models.Movie;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Table(name = "movie", schema = "pract")
public class MovieEntity
{
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "NAME", nullable = false, length = 20)
    private String name;

    @Column(name = "COUNTRY", nullable = false, length = 20)
    private String country;

    @Column(name = "DATE_", nullable = false)
    private Date date_;

    @Column(name = "DURATION", nullable = false)
    private int duration;

    @ManyToMany
    @JoinTable(
            name = "GENRE_MOVIE",
            joinColumns = @JoinColumn(name = "FK_MOVIE_ID"),
            inverseJoinColumns = @JoinColumn(name = "FK_GENRE_ID"))
    private Set<GenreEntity> movieGenres;

    @ManyToMany
    @JoinTable(
            name = "PRODUCER_MOVIE",
            joinColumns = @JoinColumn(name = "FK_MOVIE_ID"),
            inverseJoinColumns = @JoinColumn(name = "FK_PRODUCER_ID"))
    private Set<ProducerEntity> movieProducers;

    //region Getters

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getCountry()
    {
        return country;
    }

    public Date getDate_()
    {
        return date_;
    }

    public int getDuration()
    {
        return duration;
    }

    public Set<GenreEntity> getMovieGenres()
    {
        return movieGenres;
    }

    public Set<ProducerEntity> getMovieProducers()
    {
        return movieProducers;
    }

    //endregion

    //region Setters

    public void setName(String name)
    {
        this.name = name;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public void setDate_(Date date_)
    {
        this.date_ = date_;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public void setMovieGenres(Set<GenreEntity> movieGenres)
    {
        this.movieGenres = movieGenres;
    }



    public void setProducerMovies(Set<ProducerEntity> movieProducers)
    {
        this.movieProducers = movieProducers;
    }

    //endregion

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieEntity that = (MovieEntity) o;
        return id == that.id &&
                duration == that.duration &&
                Objects.equals(name, that.name) &&
                Objects.equals(country, that.country) &&
                Objects.equals(date_, that.date_);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, country, date_, duration);
    }

    public void addGenre(GenreEntity genreEntity)
    {
        this.movieGenres.add(genreEntity);
    }

    public void addProducer(ProducerEntity producerEntity)
    {
        this.movieProducers.add(producerEntity);
    }

    //region Constructors
    public MovieEntity()
    {
        this.movieGenres = new HashSet<GenreEntity>();
        this.movieProducers = new HashSet<ProducerEntity>();
    }

    public MovieEntity(Movie movie)
    {
        this.id = movie.id;
        this.name = movie.name;
        this.country = movie.country;
        this.date_ = movie.date_;
        this.duration = movie.duration;

        this.movieGenres = new HashSet<GenreEntity>();
        this.movieProducers = new HashSet<ProducerEntity>();
    }
    //endregion
}
