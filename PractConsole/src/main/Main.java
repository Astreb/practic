package main;

import console.ConsoleManager;
import console.database.DbManager;
import console.models.Genre;
import main.database.DAOService;
import main.entity.GenreEntity;

import java.util.ArrayList;

public class Main
{
    public static void main(String[] args)
    {
        ConsoleManager consoleManager = new ConsoleManager(new DAOService());
        consoleManager.work();
    }
}
