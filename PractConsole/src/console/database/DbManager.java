package console.database;

import console.models.Genre;
import console.models.Movie;
import console.models.Producer;
import interfaces.IDAOService;

import java.sql.*;
import java.util.ArrayList;


public class DbManager implements IDAOService
{
    Connection connection;
    String url;
    String user;
    String password;

    //region Properties
    public Connection getConnection()
    {
        return connection;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
    //endregion

    //region Connection
    public void initConnection()
    {
        try
        {
            //Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            connection = DriverManager.getConnection(url, user, password);
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при подключении к базе данных");
            System.out.println(ex);
        }
    }

    public void closeConnection()
    {
        try
        {
            connection.close();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при попытке закрыть подключение к базе данных");
            System.out.println(ex);
        }
    }
    //endregion

    //region AddMethods

    @Override
    public boolean addMovie(Movie movie)
    {
        // Добавление фильма
        String movieInsertQuery = "INSERT INTO MOVIE(NAME, COUNTRY, DATE_, DURATION) VALUES(?, ?, ?, ?)";

        PreparedStatement sqlStatement;
        try
        {
            sqlStatement = connection.prepareStatement(movieInsertQuery, Statement.RETURN_GENERATED_KEYS);
            sqlStatement.setString(1, movie.name);
            sqlStatement.setString(2, movie.country);
            sqlStatement.setDate(3, movie.date_);
            sqlStatement.setInt(4, movie.duration);

            sqlStatement.executeUpdate();

            ResultSet resultSet = sqlStatement.getGeneratedKeys();

            if (resultSet.next())
            {
                movie.id = resultSet.getInt(1);
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при добавлении фильма в базу данных");
            System.out.println(ex);
            return false;
        }

        // Добавление связей фильм-жанр
        String genreInsertQuery = "INSERT INTO GENRE_MOVIE (FK_MOVIE_ID, FK_GENRE_ID) VALUES (?, ?)";

        try
        {
            for (Genre genre : movie.genres)
            {
                PreparedStatement genreInsertStatement = connection.prepareStatement(genreInsertQuery);

                genreInsertStatement.setInt(1, movie.id);
                genreInsertStatement.setInt(2, genre.id);

                genreInsertStatement.executeUpdate();
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при добавлении связи фильм-жанр в базу данных");
            System.out.println(ex);
            return false;
        }

        // Добавление связей фильм-режиссёр
        String producerInsertQuery = "INSERT INTO PRODUCER_MOVIE (FK_MOVIE_ID, FK_PRODUCER_ID) VALUES (?, ?)";

        try
        {
            for (Producer producer : movie.producers)
            {
                PreparedStatement producerInsertStatement = connection.prepareStatement(producerInsertQuery);

                producerInsertStatement.setInt(1, movie.id);
                producerInsertStatement.setInt(2, producer.id);

                producerInsertStatement.executeUpdate();
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при добавлении связи фильм-режиссёр в базу данных");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean addGenre(String name)
    {
        try
        {
            String sqlQuery = "INSERT INTO GENRE (NAME) VALUES (?)";
            PreparedStatement sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setString(1, name);

            sqlStatement.executeUpdate();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при попытке добавления жанра");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean addProducer(String name)
    {
        try
        {
            String sqlQuery = "INSERT INTO PRODUCER (NAME) VALUES (?)";
            PreparedStatement sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setString(1, name);

            sqlStatement.executeUpdate();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при попытке добавления продюсера");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    //endregion

    //region SelectMethods

    @Override
    public Genre getGenreById(int genreId)
    {
        String genreSelectQuery = "SELECT ID, NAME FROM GENRE WHERE ID = ?";

        try
        {
            PreparedStatement sqlStatement = connection.prepareStatement(genreSelectQuery);
            sqlStatement.setInt(1, genreId);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next())
            {
                Genre genre = new Genre();
                genre.id = resultSet.getInt(1);
                genre.name = resultSet.getString(2);

                return genre;
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении жанра из базы данных");
            System.out.println(ex);
        }

        return null;
    }

    @Override
    public Producer getProducerById(int producerId)
    {
        String producerSelectQuery = "SELECT ID, NAME FROM PRODUCER WHERE ID = ?";

        try
        {
            PreparedStatement sqlStatement = connection.prepareStatement(producerSelectQuery);
            sqlStatement.setInt(1, producerId);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next())
            {
                Producer producer = new Producer();
                producer.id = resultSet.getInt(1);
                producer.name = resultSet.getString(2);

                return producer;
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении режиссёра из базы данных");
            System.out.println(ex);
        }

        return null;
    }

    @Override
    public int getMovieIdByName(String movieName)
    {
        String movieSelectQuery = "SELECT ID FROM MOVIE WHERE NAME = ?";

        try
        {
            PreparedStatement sqlStatement = connection.prepareStatement(movieSelectQuery);
            sqlStatement.setString(1, movieName);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next())
                return resultSet.getInt(1);
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении фильма из базы данных");
            System.out.println(ex);
        }

        return -1;
    }

    @Override
    public int getGenreIdByName(String genreName)
    {
        String genreSelectQuery = "SELECT ID FROM GENRE WHERE NAME = ?";

        try
        {
            PreparedStatement sqlStatement = connection.prepareStatement(genreSelectQuery);
            sqlStatement.setString(1, genreName);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next())
                return resultSet.getInt(1);
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении жанра из базы данных");
            System.out.println(ex);
        }

        return -1;
    }

    @Override
    public int getProducerIdByName(String producerName)
    {
        String producerSelectQuery = "SELECT ID FROM PRODUCER WHERE NAME = ?";

        try
        {
            PreparedStatement sqlStatement = connection.prepareStatement(producerSelectQuery);
            sqlStatement.setString(1, producerName);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next())
                return resultSet.getInt(1);
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении продюсера из базы данных");
            System.out.println(ex);
        }

        return -1;
    }

    @Override
    public Movie getMovieById(int movieId)
    {
        String movieSelectQuery = "SELECT ID, NAME, COUNTRY, DATE_, DURATION FROM MOVIE WHERE ID = ?";

        try
        {
            PreparedStatement sqlStatement = connection.prepareStatement(movieSelectQuery);
            sqlStatement.setInt(1, movieId);
            ResultSet resultSet = sqlStatement.executeQuery(movieSelectQuery);

            while (resultSet.next())
            {
                Movie movie = new Movie();
                movie.id = resultSet.getInt(1);
                movie.name = resultSet.getString(2);
                movie.country = resultSet.getString(3);
                movie.date_ = resultSet.getDate(4);
                movie.duration = resultSet.getInt(5);
                movie.genres = getMovieGenres(movie.id);
                movie.producers = getMovieProducers(movie.id);

                return movie;
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении фильма из базы данных");
            System.out.println(ex);
        }

        return null;
    }

    @Override
    public Movie getMovieByName(String movieName)
    {
        String movieSelectQuery = "SELECT ID, NAME, COUNTRY, DATE_, DURATION FROM MOVIE WHERE NAME = ?";

        try
        {
            PreparedStatement sqlStatement = connection.prepareStatement(movieSelectQuery);
            sqlStatement.setString(1, movieName);
            ResultSet resultSet = sqlStatement.executeQuery(movieSelectQuery);

            while (resultSet.next())
            {
                Movie movie = new Movie();
                movie.id = resultSet.getInt(1);
                movie.name = resultSet.getString(2);
                movie.country = resultSet.getString(3);
                movie.date_ = resultSet.getDate(4);
                movie.duration = resultSet.getInt(5);
                movie.genres = getMovieGenres(movie.id);
                movie.producers = getMovieProducers(movie.id);

                return movie;
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении фильма из базы данных");
            System.out.println(ex);
        }

        return null;
    }

    @Override
    public ArrayList<Genre> getMovieGenres(int movieId)
    {
        ArrayList<Genre> genres = new ArrayList<Genre>();
        String genreSelectQuery = "SELECT FK_MOVIE_ID, FK_GENRE_ID FROM GENRE_MOVIE";

        try
        {
            Statement sqlStatement = connection.createStatement();
            ResultSet resultSet = sqlStatement.executeQuery(genreSelectQuery);

            while (resultSet.next())
            {
                int id = resultSet.getInt(1);
                if (movieId != id)
                    continue;

                int genreId = resultSet.getInt(2);
                Genre genre = getGenreById(genreId);
                genres.add(genre);
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении связи жанр-фильм");
            System.out.println(ex);
        }

        return genres;
    }

    @Override
    public ArrayList<Producer> getMovieProducers(int movieId)
    {
        ArrayList<Producer> producers = new ArrayList<Producer>();
        String producerSelectQuery = "SELECT FK_MOVIE_ID, FK_PRODUCER_ID FROM PRODUCER_MOVIE";

        try
        {
            Statement sqlStatement = connection.createStatement();
            ResultSet resultSet = sqlStatement.executeQuery(producerSelectQuery);

            while (resultSet.next())
            {
                int id = resultSet.getInt(1);
                if (movieId != id)
                    continue;

                int producerId = resultSet.getInt(2);
                Producer producer = getProducerById(producerId);
                producers.add(producer);
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении связи режиссёр-фильм");
            System.out.println(ex);
        }

        return producers;
    }

    @Override
    public ArrayList<Movie> getAllMovies()
    {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        String movieSelectQuery = "SELECT ID, NAME, COUNTRY, DATE_, DURATION FROM MOVIE";

        try
        {
            Statement sqlStatement = connection.createStatement();
            ResultSet resultSet = sqlStatement.executeQuery(movieSelectQuery);

            while (resultSet.next())
            {
                Movie movie = new Movie();
                movie.id = resultSet.getInt(1);
                movie.name = resultSet.getString(2);
                movie.country = resultSet.getString(3);
                movie.date_ = resultSet.getDate(4);
                movie.duration = resultSet.getInt(5);
                movie.genres = getMovieGenres(movie.id);
                movie.producers = getMovieProducers(movie.id);

                movies.add(movie);
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении фильма из базы данных");
            System.out.println(ex);
        }

        return movies;
    }

    @Override
    public ArrayList<Genre> getAllGenres()
    {
        ArrayList<Genre> genres = new ArrayList<Genre>();

        try
        {
            String sqlQuery = "SELECT ID, NAME FROM GENRE";
            Statement sqlStatement = connection.createStatement();

            ResultSet resultSet = sqlStatement.executeQuery(sqlQuery);

            while (resultSet.next())
            {
                Genre genre = new Genre();
                genre.id = resultSet.getInt(1);
                genre.name = resultSet.getString(2);

                genres.add(genre);
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении жанра из базы данных");
            System.out.println(ex);
        }

        return genres;
    }

    @Override
    public ArrayList<Producer> getAllProducers()
    {
        ArrayList<Producer> producers = new ArrayList<Producer>();

        try
        {
            String sqlQuery = "SELECT ID, NAME FROM PRODUCER";
            Statement sqlStatement = connection.createStatement();

            ResultSet resultSet = sqlStatement.executeQuery(sqlQuery);

            while (resultSet.next())
            {
                Producer producer = new Producer();
                producer.id = resultSet.getInt(1);
                producer.name = resultSet.getString(2);

                producers.add(producer);
            }
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при получении режиссёра из базы данных");
            System.out.println(ex);
        }

        return producers;
    }

    //endregion

    //region DeleteMethods

    @Override
    public boolean deleteMovieByName(String movieName)
    {
        try
        {
            int movieId = getMovieIdByName(movieName);

            String sqlQuery = "DELETE FROM GENRE_MOVIE WHERE FK_MOVIE_ID = ?";
            PreparedStatement sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setInt(1, movieId);

            sqlStatement.executeUpdate();

            sqlQuery = "DELETE FROM PRODUCER_MOVIE WHERE FK_MOVIE_ID = ?";
            sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setInt(1, movieId);

            sqlStatement.executeUpdate();

            sqlQuery = "DELETE FROM MOVIE WHERE NAME = ?";
            sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setString(1, movieName);

            sqlStatement.executeUpdate();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при попытке удаления жанра");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean deleteGenreByName(String genreName)
    {
        try
        {
            int genreId = getGenreIdByName(genreName);

            String sqlQuery = "DELETE FROM GENRE_MOVIE WHERE FK_GENRE_ID = ?";
            PreparedStatement sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setInt(1, genreId);

            sqlStatement.executeUpdate();

            sqlQuery = "DELETE FROM GENRE WHERE NAME = ?";
            sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setString(1, genreName);

            sqlStatement.executeUpdate();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при попытке удаления жанра");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean deleteProducerByName(String producerName)
    {
        try
        {
            int producerId = getProducerIdByName(producerName);

            String sqlQuery = "DELETE FROM PRODUCER_MOVIE WHERE FK_PRODUCER_ID = ?";
            PreparedStatement sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setInt(1, producerId);

            sqlStatement.executeUpdate();

            sqlQuery = "DELETE FROM PRODUCER WHERE NAME = ?";
            sqlStatement = connection.prepareStatement(sqlQuery);
            sqlStatement.setString(1, producerName);

            sqlStatement.executeUpdate();
        }

        catch(Exception ex)
        {
            System.out.println("Ошибка при попытке удаления режиссёра");
            System.out.println(ex);
            return false;
        }

        return true;
    }

    //endregion

    //region Constructors
    public DbManager()
    {
        url = "jdbc:mysql://localhost:3306/pract?serverTimezone=Europe/Minsk&useSSL=false";
        user = "root";
        password = "18505Astreb";

        initConnection();
    }

    public DbManager(String url, String user, String password)
    {
        this.url = url;
        this.user = user;
        this.password = password;

        initConnection();
    }
    //endregion
}
