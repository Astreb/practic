package console.models;

import main.entity.GenreEntity;

public class Genre
{
    public int id;
    public String name;

    public Genre()
    {}

    public Genre(GenreEntity genre)
    {
        this.id = genre.getId();
        this.name = genre.getName();
    }

    public Genre(String genreName)
    {
        this.name = genreName;
    }
}
