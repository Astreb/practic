package console.models;

import main.entity.GenreEntity;
import main.entity.MovieEntity;
import main.entity.ProducerEntity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Set;

public class Movie
{
    public int id;
    public String name;
    public String country;
    public Date date_;
    public int duration;

    public ArrayList<Genre> genres;
    public ArrayList<Producer> producers;

    public Movie() {}

    public Movie(MovieEntity movie)
    {
        this.id = movie.getId();
        this.name = movie.getName();
        this.country = movie.getCountry();
        this.date_ = movie.getDate_();
        this.duration = movie.getDuration();

        ArrayList<Genre> genres = new ArrayList<Genre>();
        Set<GenreEntity> movieGenres = movie.getMovieGenres();
        for (GenreEntity genreEntity: movieGenres)
        {
            genres.add(new Genre(genreEntity));
        }

        ArrayList<Producer> producers = new ArrayList<Producer>();
        Set<ProducerEntity> movieProducers = movie.getMovieProducers();
        for (ProducerEntity producerEntity: movieProducers)
        {
            producers.add(new Producer(producerEntity));
        }

        this.genres = genres;
        this.producers = producers;
    }
}
