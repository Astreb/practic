package console.models;

import main.entity.ProducerEntity;

public class Producer
{
    public int id;
    public String name;

    public Producer() {}

    public Producer(ProducerEntity producer)
    {
        this.id = producer.getId();
        this.name = producer.getName();
    }

    public Producer(String producerName)
    {
        this.name = producerName;
    }
}
