package console;

import console.database.DbManager;
import console.models.Genre;
import console.models.Movie;
import console.models.Producer;
import interfaces.IDAOService;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleManager
{
    IDAOService manager;
    Scanner sc;

    //region AddMethods
    public void addGenre()
    {
        System.out.println("Введите название жанра:");

        sc.nextLine();
        String genreName = sc.nextLine();

        ArrayList<Genre> genres = manager.getAllGenres();
        for (Genre genre: genres)
            if (genre.name == genreName)
            {
                System.out.println("Жанр с таким именем уже добавлен");
                return;
            }

        if (manager.addGenre(genreName))
            System.out.println("Жанр успешно добавлен!");
    }

    public void addProducer()
    {
        System.out.println("Введите имя режиссёра:");

        sc.nextLine();
        String producerName = sc.nextLine();

        ArrayList<Producer> producers = manager.getAllProducers();
        for (Producer producer: producers)
            if (producer.name == producerName)
            {
                System.out.println("Режиссёр с таким именем уже добавлен");
                return;
            }

        if (manager.addProducer(producerName))
            System.out.println("Режиссёр успешно добавлен!");
    }

    public void addMovie()
    {
        Movie movie = new Movie();

        System.out.println("Введите название фильма:");
        sc.nextLine();
        movie.name = sc.nextLine();

        System.out.println("Введите страну фильма:");
        movie.country = sc.nextLine();

        System.out.println("Введите дату выхода фильма(yyyy-mm-dd):");
        movie.date_ = Date.valueOf(sc.nextLine());

        System.out.println("Введите продолжительность фильма(в минутах):");
        movie.duration = sc.nextInt();

        movie.genres = genreSelector();
        movie.producers = producerSelector();

        ArrayList<Movie> movies = manager.getAllMovies();
        for (Movie movie_: movies)
            if (movie_.name == movie.name)
            {
                System.out.println("Фильм с таким именем уже добавлен");
                return;
            }

        if (manager.addMovie(movie))
            System.out.println("Фильм успешно добавлен!");
    }
    //endregion

    //region DeleteMethods
    public void deleteGenre()
    {
        System.out.println("Выберите жанр:");
        int genresCount = printGenres();

        int selectedGenre = sc.nextInt();
        if (selectedGenre > genresCount)
        {
            System.out.println("Жанра с таким номером нет");
            return;
        }

        ArrayList<Genre> genres = manager.getAllGenres();
        if (manager.deleteGenreByName(genres.get(selectedGenre - 1).name))
            System.out.println("Жанр успешно удалён!");
    }

    public void deleteProducer()
    {
        System.out.println("Выберите режиссёра:");
        int producersCount = printProducers();

        int selectedProducer= sc.nextInt();
        if (selectedProducer > producersCount)
        {
            System.out.println("Режиссёра с таким номером нет");
            return;
        }

        ArrayList<Producer> producers = manager.getAllProducers();
        if (manager.deleteProducerByName(producers.get(selectedProducer - 1).name))
            System.out.println("Режиссёр успешно удалён!");
    }

    public void deleteMovie()
    {
        System.out.println("Выберите фильм:");
        int moviesCount = printMovies();

        int selectedMovie = sc.nextInt();
        if (selectedMovie > moviesCount)
        {
            System.out.println("Фильма с таким номером нет");
            return;
        }

        ArrayList<Movie> movies = manager.getAllMovies();
        if (manager.deleteMovieByName(movies.get(selectedMovie - 1).name))
            System.out.println("Фильм успешно удалён!");
    }
    //endregion

    //region PrintMethods

    public void printMenu()
    {
        System.out.println("Выберите действие:");
        System.out.println("1. Добавить жанр");
        System.out.println("2. Добавить режиссёра");
        System.out.println("3. Добавить фильм");

        System.out.println("4. Удалить жанр");
        System.out.println("5. Удалить режиссёра");
        System.out.println("6. Удалить фильм");

        System.out.println("7. Просмотр списка жанров");
        System.out.println("8. Просмотр списка режиссёров");
        System.out.println("9. Просмотр списка фильмов");

        System.out.println("0. Выход");
    }

    public int printGenres()
    {
        ArrayList<Genre> genres = manager.getAllGenres();

        int i = 1;
        for (Genre genre: genres)
        {
            System.out.println(i + ". " + genre.name);
            i++;
        }

        return genres.size();
    }

    public int printProducers()
    {
        ArrayList<Producer> producers = manager.getAllProducers();

        int i = 1;
        for (Producer producer: producers)
        {
            System.out.println(i + ". " + producer.name);
            i++;
        }

        return producers.size();
    }

    public int printMovies()
    {
        ArrayList<Movie> movies = manager.getAllMovies();

        int i = 1;
        for (Movie movie: movies)
        {
            System.out.println(i + ". " + movie.name);
            System.out.println("Страна: " + movie.country);
            System.out.println("Дата выхода: " + movie.date_);
            System.out.println("Продолжительность: " + movie.duration);

            ArrayList<String> genres = new ArrayList<String>();
            ArrayList<String> producers = new ArrayList<String>();

            for (Genre genre: movie.genres)
                genres.add(genre.name);

            for (Producer producer: movie.producers)
                producers.add(producer.name);

            System.out.println("Жанры: " + String.join(", ", genres));
            System.out.println("Режиссёры: " + String.join(", ", producers));

            System.out.println();

            i++;
        }

        return movies.size();
    }

    public static void cls() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception E) {
            System.out.println(E);
        }
    }

    //endregion

    //region SelectMethods

    public ArrayList<Genre> genreSelector()
    {
        System.out.println("Выберите жанры:");

        ArrayList<Genre> genres = manager.getAllGenres();
        printGenres();

        sc.nextLine();
        String selectedGenresLine = sc.nextLine();
        String[] selectedGenresString = selectedGenresLine.trim().split(" ");

        ArrayList<Genre> selectedGenres = new ArrayList<Genre>();
        for (String genreNumber: selectedGenresString)
        {
            selectedGenres.add(genres.get(Integer.parseInt(genreNumber) - 1));
        }

        return selectedGenres;
    }

    public ArrayList<Producer> producerSelector()
    {
        System.out.println("Выберите режиссёров:");

        ArrayList<Producer> producers = manager.getAllProducers();
        printProducers();

        String selectedProducersLine = sc.nextLine();
        String[] selectedProducersString = selectedProducersLine.trim().split(" ");

        ArrayList<Producer> selectedProducers = new ArrayList<Producer>();
        for (String genreNumber: selectedProducersString)
        {
            selectedProducers.add(producers.get(Integer.parseInt(genreNumber) - 1));
        }

        return selectedProducers;
    }

    //endregion

    //region Work

    public void work()
    {
        sc = new Scanner(System.in);
        while (true)
        {
            // cls();
            printMenu();

            int menuNumber = sc.nextInt();

            switch (menuNumber)
            {
                case 0:
                    sc.close();
                    return;

                case 1:
                    addGenre();
                    break;

                case 2:
                    addProducer();
                    break;

                case 3:
                    addMovie();
                    break;

                case 4:
                    deleteGenre();
                    break;

                case 5:
                    deleteProducer();
                    break;

                case 6:
                    deleteMovie();
                    break;

                case 7:
                    System.out.println("Жанры:");
                    printGenres();
                    break;

                case 8:
                    System.out.println("Режиссёры:");
                    printProducers();
                    break;

                case 9:
                    System.out.println("Фильмы:");
                    printMovies();
                    break;
            }

        }
    }

    //endregion

    //region Contructors

    public ConsoleManager()
    {
        manager = new DbManager();
    }

    public ConsoleManager(IDAOService dbManager)
    {
        manager = dbManager;
    }

    //endregion
}
