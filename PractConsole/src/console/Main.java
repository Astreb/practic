package console;


import console.database.DbManager;

public class Main
{
    public static void main(String[] args)
    {
        DbManager dbManager = new DbManager();
        ConsoleManager consoleManager = new ConsoleManager(dbManager);
        consoleManager.work();
        dbManager.closeConnection();
    }
}
